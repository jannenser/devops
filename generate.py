import random
import pickle
import argparse
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument('--model', required=True, type=str, nargs=1)
# если я не задаю seed при вводе, то просто уберу аргумент....
parser.add_argument('--length', required=True, type=int, default=[200])
parser.add_argument('--seed', type=str)
args = parser.parse_args()
# Загружаем файл
file = open(args.model[0], mode='rb')
module = pickle.load(file)

# Тот же сет со всеми различными словами текста text
all_words = module.keys()
hop = list(all_words)
seq = {}.fromkeys(all_words)
# Создаем новую последовательность слов, идущих после данного
for word in all_words:
    new_next = []
    # Когда после слова ничего нет, просто добавляем все различные слова)0))0))
    if module[word] is None:
        seq.update({word: hop})
    else:
        for k, count in module[word].items():
            count = module[word][k]
            # По количеству повторений (count) словосочетаний слово1-слово2
            # выписываем слово2 count раз
            new_next.extend([k] * count)
        seq[word] = new_next

length = args.length

all_words = list(module.keys())
if args.seed is None:
    seed = random.choice(all_words)
else:
    seed = args.seed
for i in range(length):
    print(seed, end=' ')
    # После заданного первого слова выводим последовательность
    # из возможных слов с помощью random.choice
    try:
        seed = random.choice(seq[seed])
    except:
        print("There is no written seed-word")
        exit(1)