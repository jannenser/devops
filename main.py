#!/usr/bin/python
# -*- coding: utf-8 -*-

from train import *

parser = argparse.ArgumentParser()
parser.add_argument('--input-dir', default=['stdin'])
parser.add_argument('--model', required=True, type=str)
parser.add_argument('--lc', type=bool, nargs='?', default=False)
args = parser.parse_args()
directory = args.input_dir[0]

    # Почему-то с "if directory is None" не катит
if args.input_dir[0] == 'stdin':
#if directory is None:
    # Случай, когда ввод происходит с stdin
    strings = sys.stdin.read().split('\n')
    module = create_module(strings)

    # Случай, когда текстовые файлы в директории есть
else:
    files = os.listdir(directory)
    # Ищем только текстовые файлы
    texts = filter(lambda x: x.endswith('.txt'), files)
    for file in texts:
        f = open(file, 'r')
        module = create_module(f)

# Сохраняем в файл с помощью pickle, чтобы потом можно было достать в generate
outp = open(args.model, mode='wb')
pickle.dump(module, outp)
outp.close()