# -*- codong: utf-8 -*-
import numpy as np
import pandas as pd
import train 

def test_numpy():
    assert(np.random.random_sample() < 2)

def test_clean_line():
    line = 'pen, apple and pinapple!'
    assert(train.clean(line) == ['pen', 'apple', 'and', 'pinapple', ''])

def test_last_word_add():
    text = ['let', 'it', 'be', 'let', 'it', 'be']
    module = {'let': {}, 'it': {}, 'be': {}}
    new_module = train.check_last_word(text, module)
    assert(new_module['it'] == {'be': 2})

def test_empty_text():
    text = []
    module = {}
    assert(len(train.check_last_word(text, module)) < 1)

def test_empty_creation():
    new_text = []
    module = train.create_module(new_text)
    assert(len(module) == 0)

def test_simple_text():
    text = []
    module = dict()
    new_text = ['Georgia, Georgia', 'The whole day through', 'Just an old sweet song', 'Keeps Georgia on my ming']
    for line in new_text:
        line = train.clean(line.lower())
        for word in line:
            text.append(word)
        train.create_dict(line, module)
    new_module = train.check_last_word(text, module)
    assert(len(new_module['georgia']) == 3)
    