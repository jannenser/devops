# -*- codong: utf-8 -*-
import train
import pandas as pd
import numpy as np

def test_blabla():
    assert(10 + 10 == 20)

def test_pandas():
    s = pd.Series(np.random.randn(5), index=['a', 'b', 'c', 'd', 'e'])
    assert(s['c'] < 6)