#!/usr/bin/python
# -*- coding: utf-8 -*-

import re
import pickle
import argparse
import sys
import os
import numpy as np
import pandas as pd

def clean(line):
    return re.split(r"\W+", line)

def clean_sentence(line):
    # Очистка текста
    tmp = clean(line)
    for i in tmp:
        if i != '':
            # Записываем слова в массив предложения text
            global text
            text.append(i)
            # module - словарь словарей (я не понимаю, зачем здесь defaultdict)
            global module
            if i not in module.keys():
                module.update({i: None})


def check_last_word(text, module):
    if len(text) != 0:
        # Меняем последнее слово
        last = text[-1]
    for i in range(len(text) - 1):
        if module[text[i]] is None:
            next = {text[i + 1]: 1}
            module.update({text[i]: next})
        else:
            next = module[text[i]]
            if text[i + 1] in next.keys():
                next[text[i + 1]] += 1
            else:
                next.update({text[i + 1]: 1})
            module.update({text[i]: next})
    return module

# Делаем словарь из данного файла с текстом
def create_module(f):
    module = dict()
    # последнее слово в строке
    last = ''
    for line in f:
        # Пусть теперь text - это строка файла
        text = []
        if args.lc == False:
            line = line.lower()
        clean_sentence(line)
        # Проверка последнего слова в строке
        if (last != '' and len(text) != 0):
            if module[last] is None:
                    next = {text[0]: 1}
                    module.update({last: next})
            else:
                next = module[last]
                if text[0] in next.keys():
                    next[text[0]] += 1
                else:
                    next.update({text[0]: 1})
                module.update({last: next})
        module = check_last_word(text, module)
    return module

def create_dict(line, module):
    for word in line:
        module.update({word:{}})
